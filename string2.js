// ==== String Problem #2 ====
// Given an IP address - "111.139.161.143". Split it into its component parts 111, 139, 161, 143 and return it in an array in numeric values. 
// [111, 139, 161, 143].

// Support IPV4 addresses only. If there are other characters detected, return an empty array.

//4 decimal number .Range of number is 0-255.
function string2(input){
    let array=input.split('.');
    let finalArray=[];
    let string;//Temporary variable
    let number;//Temporary variable to store number in the form of integer as a integer
    for(let index=0;index<array.length;index++){
        number=parseInt(array[index]);
        if(number.toString().length === array[index].length && !(Number.isNaN(number))){
            finalArray.push(number);
        }
        else{
            return [];
        }
    }
    if(finalArray.length===4){
        return finalArray;
    }
    else{
        return [];
    }
}
module.exports={string2};